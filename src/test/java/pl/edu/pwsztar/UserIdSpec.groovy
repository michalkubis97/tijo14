package pl.edu.pwsztar

import spock.lang.Specification

class UserIdSpec extends Specification {

    def "check incorrect size"(){
        given: UserId userId = new UserId("9705670765")
        when: boolean correct = userId.isCorrectSize()
        then:
        !correct
    }

}
