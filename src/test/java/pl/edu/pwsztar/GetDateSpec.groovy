package pl.edu.pwsztar

import spock.lang.Specification

class GetDateSpec extends Specification {
    def "read date from id"(){
        given: "user id"
        UserId userId = new UserId("89764567551")
        UserId userId2 = new UserId("97033156587")
        when: "call function"
        String date = userId.getDate().get()
        String date2 = userId2.getDate().get()
        then: "checking"
        date == "15-06-1989"
        date2 == "31-03-1997"
    }
}
